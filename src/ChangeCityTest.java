import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ChangeCityTest {
	public  String baseURL = "https://mister.am/#";
	public WebDriver webDriver;
	
	
	@BeforeTest
	public void initBrowser() {

		System.setProperty("webdriver.ie.driver","D:\\selenium\\IEDriverServer.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false); capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
		capabilities.setCapability("allow-blocked-content", true);
		webDriver = new InternetExplorerDriver(capabilities);
		webDriver.get(baseURL);
		
	}
	
	@Test
	public void selectionCityTest() {
		
		 webDriver.findElement(By.xpath("//h1[@class='js-redirect-page']//span")).click();
				
		WebElement selectedCityInList = webDriver.findElement(By.xpath("//div[@id='ngdialog8']//li[8]//a[1]"));
		String cityInList = selectedCityInList.getAttribute("data-city_url");
		selectedCityInList.click();
		System.out.println(cityInList);
		
		WebElement changedCityElement = webDriver.findElement(By.xpath("//h1[@class='js-redirect-page']//span"));
	    String changedCity =  changedCityElement.getAttribute("data-href");
	    changedCity = changedCity.substring(1);
	    System.out.println(changedCity);
	    
	   	if (cityInList.equalsIgnoreCase(changedCity)) 
	    		System.out.println("Selected city is " + changedCity);
	   	else
	    		System.out.println("City does not correspond to selected one");
	       	
	}
	
	@AfterTest
	public void discontinueBrowser() {
		webDriver.close();
	}

}
