import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class ChartTest {
	
	public  String baseURL = "https://mister.am";
	public WebDriver webDriver;
	
	
	@BeforeTest
	public void initBrowser() {
		System.setProperty("webdriver.ie.driver","D:\\selenium\\IEDriverServer.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false); capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
		capabilities.setCapability("allow-blocked-content", true);
		webDriver = new InternetExplorerDriver(capabilities);
		webDriver.get(baseURL);
		WebDriverWait wait = new WebDriverWait(webDriver, 3);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("chatra")));
		
	}
	
	@Test	
	public void checkingChartTest() {
		
		WebElement chartButton = webDriver.findElement(By.xpath("//div[@class='bg-top-image']/div[2]"));
		chartButton.click();
		System.out.println(webDriver.findElement(By.id("chatra")).isDisplayed());
	}
	
	@AfterTest
	public void discontinueBrowser() {
		webDriver.close();
	}

}
